const path = require('path');

module.exports = {
	entry: './client/app.js',
	output: {
		filename: "bundle.js",
		path: path.resolve(__dirname, 'dist'),
		publicPath: "/dist/"
	},
	module: {
		rules: [
			{
				test: /\.css$/,
				use: [ 'style-loader', 'css-loader' ]
			}
		],
		loaders: [{
				test: /\.js?$/,
				exclude: /node_modules/,
				loader: 'babel-loader',
			}, {
				test: /\.css$/,
				loader: "style-loader!css-loader"
		}]
	}
};