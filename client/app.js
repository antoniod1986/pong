import './style.css';
import loginComponent from './components/login/loginComponent';
import gameComponent from './components/game/gameComponent';
import SocketSingleton from './libs/SocketSingleton';
import CoreSingleton from './libs/CoreSingleton';
import Ball from "../shared/classes/Ball";
import loadingComponent from "./components/loading/loadingComponent";
import Vector from "../shared/classes/Vector";
import Paddle from "./classes/Paddle";
import TennisBall from "./classes/TennisBall";

function init() {
	const login = new loginComponent('.login-component');
	let game;
	let loading = null;
	const sock = SocketSingleton.instance.socket;
	const core = CoreSingleton.instance;
	let user = null;

	sock.on('connected', function(res){
		console.log('connected', res.id);
	});

	sock.on('login successful', function(res) {
		console.log('login successful', res);
		user = res;
		if(!gameComponent.instantiated) {
			loading = new loadingComponent('.loading-component', user);
		}
	});

	sock.on('start game', function(data) {
		if(loading!==null) {
			loading.destroy();
		}
		console.log("START GAME", data);
		game = new gameComponent('.game-component', data);
		core.init(
			game.context,
			new Paddle(new Vector( data.playerOne.position.x, data.playerOne.position.y ), new Vector(), data.playerOne.sockID, data.playerOne.user, data.playerOne.roomID, ( data.playerAssigned == 'playerTwo') ),
			new Paddle(new Vector( data.playerTwo.position.x , data.playerTwo.position.y ), new Vector(), data.playerTwo.sockID, data.playerTwo.user, data.playerTwo.roomID, ( data.playerAssigned == 'playerOne') ),
			new TennisBall(new Vector( Ball.DEFAULT_X, Ball.DEFAULT_Y ), new Vector(), Ball.DEFAULT_RADIUS, Ball.DEFAULT_START_ANGLE, Ball.DEFAULT_END_ANGLE),
			data.playerAssigned);
		game.startKeyboardTrace();
	});
}

window.onload = init;