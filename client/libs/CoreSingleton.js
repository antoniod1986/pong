import SocketSingleton from './SocketSingleton';
import Game from "../../shared/classes/Game";

let _core_singleton = Symbol();

export default class CoreSingleton {
	constructor(coreToken) {
		if (_core_singleton !== coreToken)
			throw new Error('Cannot instantiate directly.');
	}

	static get instance() {
		if(!this[_core_singleton])
			this[_core_singleton] = new CoreSingleton(_core_singleton);
		return this[_core_singleton]
	}

	init( CanvasContext, PlayerOne, PlayerTwo, Ball, playerAssigned ) {
		if(CoreSingleton.initialized) {
			return;
		}
		CoreSingleton.sock = SocketSingleton.instance.socket;
		CoreSingleton.initialized = true;
		CoreSingleton.canvasContext = CanvasContext;
		this.playerOne = PlayerOne;
		this.playerTwo = PlayerTwo;
		this.ball = Ball;
		this.ball.initListening();
		this.playerAssigned = playerAssigned;

		this.animate(this.step);
	}

	animate(callback) {
		const that = this;
		window.setTimeout(function() {
				callback(that);
		},
		CoreSingleton.FPS);
	}


	step(that) {
		that.update(that);
		that.clear();
		that.render(that);
		that.animate(that.step);
	}

	update(that) {
		that.playerOne.update();
		that.playerTwo.update();
		that.ball.update();
	}

	clear() {
		CoreSingleton.canvasContext.clearRect( 0, 0, Game.CANVAS_WIDTH, Game.CANVAS_HEIGHT );
	}

	render(that) {
		that.playerOne.render();
		that.playerTwo.render();
		that.ball.render();
	}


}

CoreSingleton.initialized = false;
CoreSingleton.canvasContext = null;
CoreSingleton.FPS = 1000/60;
CoreSingleton.sock = null;
