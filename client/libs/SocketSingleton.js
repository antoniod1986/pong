import io from 'socket.io-client';

let _socket_singleton = Symbol();

export default class SocketSingleton {
	constructor(socketToken) {
		if (_socket_singleton !== socketToken)
			throw new Error('Cannot instantiate directly.');

		this._socket = io();
	}

	static get instance() {
		if(!this[_socket_singleton])
			this[_socket_singleton] = new SocketSingleton(_socket_singleton);
		return this[_socket_singleton]
	}

	get socket() {
		return this._socket;
	}
}