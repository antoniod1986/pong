export default class Component {
	constructor(element) {
		this.onBeforeInit();
		this._elementRef = element;
		this.onInit();
		this.onAfterInit();
	};

	get elementRef() {
		return this._elementRef;
	}

	render(html) {
		document.querySelector(this._elementRef).insertAdjacentHTML('beforeend', html);
	}

	bind(el = '', ev, callback, preventDefault = false) {
		document.querySelector(this._buildElement(el)).addEventListener(ev, (event) => {
			if(preventDefault) {
				event.preventDefault();
			}
			callback.call(this, event);
		});
	};

	showComponent() {
		document.querySelector(this._elementRef).style.display = 'block';
	}

	hideComponent() {
		document.querySelector(this._elementRef).style.display = 'none';
	}

	destroy() {
		document.querySelector(this._elementRef).innerHTML = '';
	}
	
	_buildElement(el) {
		return this._elementRef + ' ' + el;
	};

	onBeforeInit() {
		// Override...
	}

	onInit() {
		// Override...
	}

	onAfterInit() {
		// Override...
	}


}