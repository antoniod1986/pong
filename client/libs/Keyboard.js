export default class Keyboard {
	constructor() {
		this.startTrace();
	};

	startTrace() {
		window.addEventListener('keydown',
			function(e) {
				var code = e.keyCode;
				switch (code) {
					case 38:
						Keyboard.direction = -1;
						break;
					case 40:
						Keyboard.direction = 1;
						break;
				}
			},false);
		window.addEventListener('keyup',
			function(e) {
				Keyboard.direction = 0;
			},false);
	}
}

Keyboard.direction = 0;