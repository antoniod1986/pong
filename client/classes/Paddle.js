import CoreSingleton from "../libs/CoreSingleton";
import Keyboard from "../libs/Keyboard";
import Player from "../../shared/classes/Player";
import Game from "../../shared/classes/Game";
import SocketSingleton from "../libs/SocketSingleton";
import Vector from "../../shared/classes/Vector";

export default class Paddle extends Player {
	constructor( position, velocity, sockID, user, roomID, opponent = false , width = Player.DEFAULT_WIDTH, height = Player.DEFAULT_HEIGHT) {
		super( position, velocity, sockID, user, roomID );
		this.width = width;
		this.height = height;
		this.opponent = opponent;
		this.sock = SocketSingleton.instance.socket;
		const that = this;

		if( this.opponent ) {
			this.sock.on( 'update opponent direction', function( opponentPlayer ) {
				that.position = opponentPlayer.position;
			});
			this.sock.on('increment score', function( data ) {
				that.score = data.playerTwo;
			});
		} else {
			this.sock.on('increment score', function( data ) {
				that.score = data.playerOne;
			});
		}
	}

	render() {
		const ctx = CoreSingleton.canvasContext;
		ctx.beginPath();
		ctx.rect( this.position.x, this.position.y, this.width, this.height );
		ctx.fillStyle = '#ffffff';
		ctx.fill();
	}

	update() {
		const direction = Keyboard.direction;
		if( !this.opponent ) {
			this.checkCollisionsAndSetPosition( direction );
			this.sock.emit( 'update my direction', direction );
		}
	}
}