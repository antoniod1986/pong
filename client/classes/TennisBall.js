import CoreSingleton from "../libs/CoreSingleton";
import Ball from "../../shared/classes/Ball";
import SocketSingleton from "../libs/SocketSingleton";

export default class TennisBall extends Ball {
	constructor( position, velocity, radius = Ball.DEFAULT_RADIUS, startAngle = Ball.DEFAULT_START_ANGLE, endAngle = Ball.DEFAULT_END_ANGLE ) {
		super( position, velocity );
		this.radius = radius;
		this.startAngle = startAngle;
		this.endAngle = endAngle;
		this.sock = SocketSingleton.instance.socket;
	}

	initListening() {
		let that = this;
		this.sock.on('update ball direction', function(ballData) {
			that.position = ballData.position;
			that.velocity = ballData.velocity;
		});
	}

	render() {
		const ctx = CoreSingleton.canvasContext;
		ctx.beginPath();
		ctx.arc(this.position.x, this.position.y, this.radius, this.startAngle, this.endAngle);
		ctx.fillStyle = '#ffffff';
		ctx.fill();
	}

	update() {

	}
}