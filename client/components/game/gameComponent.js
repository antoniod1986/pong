import Component from '../../libs/Component';
import './gameComponent.css';
import Keyboard from "../../libs/Keyboard";
import SocketSingleton from '../../libs/SocketSingleton';

export default class gameComponent extends Component {
	constructor(element, data) {
		gameComponent.instantiated = true;
		const sock = SocketSingleton.instance.socket;
		super(element);
		const that = this;

		this.render(`
			<h3 class="player player-one">${data.playerOne.user}  <span class="player-one__score">${data.playerOne.score}</span></h3>
			<canvas class="game-canvas" width="500" height="400"></canvas>
			<h3 class="player player-two"><span class="player-two__score">${data.playerTwo.score}</span>  ${data.playerTwo.user}</h3>
		`);

		sock.on('increment score', function( data ) {
			const playerOneSelector = '.player-one__score';
			const playerTwoSelector = '.player-two__score';
			document.querySelector(playerOneSelector).innerText = data.playerOne;
			document.querySelector(playerTwoSelector).innerText = data.playerTwo;
		});
	}

	get canvas() {
		return document.querySelector(this.elementRef + " canvas");
	}

	get context() {
		return this.canvas.getContext("2d");
	}

	startKeyboardTrace() {
		const kyeboard = new Keyboard();
	}
}

gameComponent.instantiated = false;