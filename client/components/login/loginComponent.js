import Component from '../../libs/Component';
import './loginComponent.css';
import SocketSingleton from '../../libs/SocketSingleton';

export default class loginComponent extends Component {
	constructor(element) {
		super(element);
		const that = this;
		const sock = SocketSingleton.instance.socket;

		this.render(`
			<form class="login-form">
				<input class="login-form__user" type="text" placeholder="Player_1" pattern="^[A-Za-z0-9_-]{3,16}$" autofocus>
			</form>
		`);

		this.bind(
			'.login-form',
			'submit',
			function(ev) {
				const user_input = document.querySelector(that.elementRef + ' .login-form__user');
				const user_val = user_input.value;
				sock.emit('login', {"user": user_val});

				sock.on('login successful', function(res) {
					that.hideComponent();
					that.destroy();
				});
			},
			true);
	}
}