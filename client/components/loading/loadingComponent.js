import Component from '../../libs/Component';
import './loadingComponent.css';
import SocketSingleton from '../../libs/SocketSingleton';

export default class loadingComponent extends Component {
	constructor(element, data) {
		super(element);
		const that = this;
		const sock = SocketSingleton.instance.socket;

		this.render(`
			<h3>USER: ${data.user}</h3>
			<h3>ROOM: ${data.roomID}</h3>
			<p>Waiting for other user connection...</p>
		`);

		sock.on('start game', function(data) {
			that.destroy();
		});
	}
}