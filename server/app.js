import Vector from "../shared/classes/Vector";

var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var path = require('path');
import UUID from 'uuid';

import Game from "../shared/classes/Game";
import Player from './../shared/classes/Player';
import Ball from "../shared/classes/Ball";

var users = [];
var rooms = [];
var room_number = 0;
var sockets = {};
var port = process.env.PORT || 3000;


app.use(express.static(path.join('../')));
app.get('/', function(req, res){
	res.sendFile( path.resolve('index.html') );
});
app.get('/dist/bundle.js', function(req, res){
	res.sendFile( path.resolve('dist/bundle.js') );
});

io.on('connection', function(socket){
	socket.id = UUID();
	console.log('a user connected: ', socket.id);
	socket.emit('connected', {"id" : socket.id});

	socket.on('disconnect', function(user){
		users.filter(function (user) {
			return user.sockID !== socket.id;
		});
		console.log(socket.id, ' user has been disconnected');
	})

	socket.on('login', function(res){
		const new_user = new Player( new Vector( 0, Player.DEFAULT_Y ), new Vector(), socket.id, res.user, null );
		users.push(new_user);
		sockets[users[users.length-1].sockID] = socket;

		users[users.length-1].roomID = assignRoom(users[users.length-1]);
		socket.emit('login successful', new_user);
	});

});

function assignRoom(player) {
	player.position.y = Player.DEFAULT_Y;
	if(rooms.length===0) {
		player.position.x = Player.DEFAULT_X_PLAYER_ONE;
		rooms.push({
			'roomID' : generateRoomName(),
			'playerOne' : player
		});
	} else if(rooms[rooms.length-1].playerTwo!==undefined) {
		player.position.x = Player.DEFAULT_X_PLAYER_ONE;
		rooms.push({
			'roomID' : generateRoomName(),
			'playerOne' : player
		});
	} else {
		player.position.x = Player.DEFAULT_X_PLAYER_TWO;
		player.opponent = true;
		rooms[rooms.length-1].playerTwo = player;
		createGame(rooms[rooms.length-1]);
	}

	return rooms[rooms.length-1].roomID;
}

function generateRoomName() {
	return 'room_' + room_number++;
}

function getRoomBySockID(sockID) {
	const currentUser = users.find(function (user) {
		return user.sockID === sockID;
	}) || {};
	return currentUser.roomID || '';
}

function createGame(room) {
	room.ball = new Ball( new Vector( Ball.DEFAULT_X, Ball.DEFAULT_Y ), Ball.getRandomVelocity() );
	const game = new Game( room.playerOne, room.playerTwo, room.roomID, room.ball );
	let intervalID = null;

	sockets[room.playerOne.sockID].emit('start game', {
		'playerOne': room.playerOne,
		'playerTwo': room.playerTwo,
		'room': room.roomID,
		'playerAssigned': 'playerOne'
	});

	sockets[room.playerTwo.sockID].emit('start game', {
		'playerOne': room.playerOne,
		'playerTwo': room.playerTwo,
		'room': room.roomID,
		'playerAssigned': 'playerTwo'
	});

	sockets[room.playerOne.sockID].on('update my direction', function( shift ) {
		game.playerOne.checkCollisionsAndSetPosition( shift );
		sockets[room.playerTwo.sockID].emit( 'update opponent direction', game.playerOne );
	});
	sockets[room.playerTwo.sockID].on('update my direction', function( shift ) {
		game.playerTwo.checkCollisionsAndSetPosition( shift );
		sockets[room.playerOne.sockID].emit('update opponent direction', game.playerTwo );
	});

	setInterval(function() {
		game.ball.checkCollisionsAndSetPosition( game.playerOne, game.playerTwo );
		sockets[room.playerOne.sockID].emit('increment score', {
			'playerOne': game.playerOne.score,
			'playerTwo': game.playerTwo.score
		});
		sockets[room.playerTwo.sockID].emit('increment score', {
			'playerOne': game.playerOne.score,
			'playerTwo': game.playerTwo.score
		});
		sockets[room.playerOne.sockID].emit('update ball direction', {
			position: game.ball.position,
			velocity: game.ball.velocity
		});
		sockets[room.playerTwo.sockID].emit('update ball direction', {
			position: game.ball.position,
			velocity: game.ball.velocity
		});
	},
		Game.FPS);
}

http.listen(port, function() {
	console.log('listening on *:' + port);
});