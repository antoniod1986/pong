import Player from "../../shared/classes/Player";

export default class Room {
	constructor( roomID, playerOne = null, playerTwo = null ) {
		this.roomID = roomID;
		this.playerOne = playerOne;
		this.playerTwo = playerTwo;

		Room.rooms.push(this);
	}

	static generateRoomName() {
		return 'room_' + Room.rooms.length;
	}

	getRoomBySockID( sockID ) {
		const currentUser = users.find(function (user) {
			return user.sockID === sockID;
		}) || {};
		return currentUser.roomID || '';
	}

	static assignRoom( player ) {
		if( !Room.isLastRoomFull() ) {
			player.position.x = Player.DEFAULT_X_PLAYER_ONE;
			new Room( Room.generateRoomName(), player );
		} else {
			player.position.x = Player.DEFAULT_X_PLAYER_TWO;
			player.opponent = true;
			Room.getTopRoom().playerTwo = player;
			createGame(rooms[rooms.length-1]);
		}

		return rooms[rooms.length-1].roomID;
	}

	static getTopRoom() {
		return Room.rooms[Room.rooms.length-1];
	}

	static isLastRoomFull() {
		let isFull = false;
		try {
			isFull = ( Room.getTopRoom().playerTwo !== null );
		} catch( e ) {
			isFull = false;
		}
		return isFull;
	}
}

Room.rooms = [];
