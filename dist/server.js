/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 4);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
class Vector {
	constructor( x = 0, y = 0, module ) {
		this.x = x;
		this.y = y;
	}

	negative() {
		return new Vector( -this.x, -this.y );
	}

	add( v ) {
		return new Vector( this.x + v.x, this.y + v.y );
	}

	substract ( v ) {
		return new Vector( this.x + v.x, this.y + v.y );
	}

	multiply ( v ) {
		return new Vector( this.x * v.x, this.y * v.y );
	}

	divide ( v ) {
		return new Vector( this.x / v.x, this.y / v.y );
	}
}
/* harmony export (immutable) */ __webpack_exports__["a"] = Vector;


/***/ }),
/* 1 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
class Game {
	constructor( playerOne, playerTwo, room, ball ) {
		this.playerOne = playerOne;
		this.playerTwo = playerTwo;
		this.room = room;
		this.ball = ball;
	}

	init() {
		const that = this;
		setTimeout(function() {

		},
			Game.FPS);
	}
}
/* harmony export (immutable) */ __webpack_exports__["a"] = Game;


Game.FPS = 1000/60;
Game.CANVAS_WIDTH = 500;
Game.CANVAS_HEIGHT = 400;

/***/ }),
/* 2 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Particle__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__Vector__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__Game__ = __webpack_require__(1);




class Player extends __WEBPACK_IMPORTED_MODULE_0__Particle__["a" /* default */] {
	constructor(position, velocity, sockID, user, roomID) {
		super(position, velocity);
		this.sockID = sockID;
		this.user = user;
		this.roomID = roomID;
		this.score = 0;
	}

	checkCollisionsAndSetPosition(direction) {
		let new_velocity = new __WEBPACK_IMPORTED_MODULE_1__Vector__["a" /* default */]( 0, Player.DEFAULT_STEP * direction );
		let new_position = this.position.add( new_velocity );

		if( Player.DEFAULT_HEIGHT + new_position.y >= __WEBPACK_IMPORTED_MODULE_2__Game__["a" /* default */].CANVAS_HEIGHT ) {
			new_position.y = __WEBPACK_IMPORTED_MODULE_2__Game__["a" /* default */].CANVAS_HEIGHT - Player.DEFAULT_HEIGHT - Player.DEFAULT_STEP;
		} else if( new_position.y <= 0 ) {
			new_position.y = Player.DEFAULT_STEP;
		}

		this.position = new_position;
	}
}
/* harmony export (immutable) */ __webpack_exports__["a"] = Player;


Player.DEFAULT_WIDTH = 15;
Player.DEFAULT_HEIGHT = 80;
Player.DEFAULT_X_PLAYER_ONE = 5;
Player.DEFAULT_X_PLAYER_TWO = 480;
Player.DEFAULT_Y = 160;
Player.DEFAULT_STEP = 5;

/***/ }),
/* 3 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
class Particle {
	constructor( position, velocity ) {
		this.position = position;
		this.velocity = velocity;
	}

	checkCollisionsAndSetPosition() {
		// override
	}
}
/* harmony export (immutable) */ __webpack_exports__["a"] = Particle;


/***/ }),
/* 4 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__shared_classes_Vector__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_uuid__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_uuid___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_uuid__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shared_classes_Game__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_classes_Player__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__shared_classes_Ball__ = __webpack_require__(10);


var express = __webpack_require__(5);
var app = express();
var http = __webpack_require__(6).Server(app);
var io = __webpack_require__(7)(http);
var path = __webpack_require__(8);






var users = [];
var rooms = [];
var room_number = 0;
var sockets = {};
var port = process.env.PORT || 3000;


app.use(express.static(path.join('../')));
app.get('/', function(req, res){
	res.sendFile( path.resolve('index.html') );
});
app.get('/dist/bundle.js', function(req, res){
	res.sendFile( path.resolve('dist/bundle.js') );
});

io.on('connection', function(socket){
	socket.id = __WEBPACK_IMPORTED_MODULE_1_uuid___default()();
	console.log('a user connected: ', socket.id);
	socket.emit('connected', {"id" : socket.id});

	socket.on('disconnect', function(user){
		users.filter(function (user) {
			return user.sockID !== socket.id;
		});
		console.log(socket.id, ' user has been disconnected');
	})

	socket.on('login', function(res){
		const new_user = new __WEBPACK_IMPORTED_MODULE_3__shared_classes_Player__["a" /* default */]( new __WEBPACK_IMPORTED_MODULE_0__shared_classes_Vector__["a" /* default */]( 0, __WEBPACK_IMPORTED_MODULE_3__shared_classes_Player__["a" /* default */].DEFAULT_Y ), new __WEBPACK_IMPORTED_MODULE_0__shared_classes_Vector__["a" /* default */](), socket.id, res.user, null );
		users.push(new_user);
		sockets[users[users.length-1].sockID] = socket;

		users[users.length-1].roomID = assignRoom(users[users.length-1]);
		socket.emit('login successful', new_user);
	});

});

function assignRoom(player) {
	player.position.y = __WEBPACK_IMPORTED_MODULE_3__shared_classes_Player__["a" /* default */].DEFAULT_Y;
	if(rooms.length===0) {
		player.position.x = __WEBPACK_IMPORTED_MODULE_3__shared_classes_Player__["a" /* default */].DEFAULT_X_PLAYER_ONE;
		rooms.push({
			'roomID' : generateRoomName(),
			'playerOne' : player
		});
	} else if(rooms[rooms.length-1].playerTwo!==undefined) {
		player.position.x = __WEBPACK_IMPORTED_MODULE_3__shared_classes_Player__["a" /* default */].DEFAULT_X_PLAYER_ONE;
		rooms.push({
			'roomID' : generateRoomName(),
			'playerOne' : player
		});
	} else {
		player.position.x = __WEBPACK_IMPORTED_MODULE_3__shared_classes_Player__["a" /* default */].DEFAULT_X_PLAYER_TWO;
		player.opponent = true;
		rooms[rooms.length-1].playerTwo = player;
		createGame(rooms[rooms.length-1]);
	}

	return rooms[rooms.length-1].roomID;
}

function generateRoomName() {
	return 'room_' + room_number++;
}

function getRoomBySockID(sockID) {
	const currentUser = users.find(function (user) {
		return user.sockID === sockID;
	}) || {};
	return currentUser.roomID || '';
}

function createGame(room) {
	room.ball = new __WEBPACK_IMPORTED_MODULE_4__shared_classes_Ball__["a" /* default */]( new __WEBPACK_IMPORTED_MODULE_0__shared_classes_Vector__["a" /* default */]( __WEBPACK_IMPORTED_MODULE_4__shared_classes_Ball__["a" /* default */].DEFAULT_X, __WEBPACK_IMPORTED_MODULE_4__shared_classes_Ball__["a" /* default */].DEFAULT_Y ), __WEBPACK_IMPORTED_MODULE_4__shared_classes_Ball__["a" /* default */].getRandomVelocity() );
	const game = new __WEBPACK_IMPORTED_MODULE_2__shared_classes_Game__["a" /* default */]( room.playerOne, room.playerTwo, room.roomID, room.ball );
	let intervalID = null;

	sockets[room.playerOne.sockID].emit('start game', {
		'playerOne': room.playerOne,
		'playerTwo': room.playerTwo,
		'room': room.roomID,
		'playerAssigned': 'playerOne'
	});

	sockets[room.playerTwo.sockID].emit('start game', {
		'playerOne': room.playerOne,
		'playerTwo': room.playerTwo,
		'room': room.roomID,
		'playerAssigned': 'playerTwo'
	});

	sockets[room.playerOne.sockID].on('update my direction', function( shift ) {
		game.playerOne.checkCollisionsAndSetPosition( shift );
		sockets[room.playerTwo.sockID].emit( 'update opponent direction', game.playerOne );
	});
	sockets[room.playerTwo.sockID].on('update my direction', function( shift ) {
		game.playerTwo.checkCollisionsAndSetPosition( shift );
		sockets[room.playerOne.sockID].emit('update opponent direction', game.playerTwo );
	});

	setInterval(function() {
		game.ball.checkCollisionsAndSetPosition( game.playerOne, game.playerTwo );
		sockets[room.playerOne.sockID].emit('increment score', {
			'playerOne': game.playerOne.score,
			'playerTwo': game.playerTwo.score
		});
		sockets[room.playerTwo.sockID].emit('increment score', {
			'playerOne': game.playerOne.score,
			'playerTwo': game.playerTwo.score
		});
		sockets[room.playerOne.sockID].emit('update ball direction', {
			position: game.ball.position,
			velocity: game.ball.velocity
		});
		sockets[room.playerTwo.sockID].emit('update ball direction', {
			position: game.ball.position,
			velocity: game.ball.velocity
		});
	},
		__WEBPACK_IMPORTED_MODULE_2__shared_classes_Game__["a" /* default */].FPS);
}

http.listen(port, function() {
	console.log('listening on *:' + port);
});

/***/ }),
/* 5 */
/***/ (function(module, exports) {

module.exports = require("express");

/***/ }),
/* 6 */
/***/ (function(module, exports) {

module.exports = require("http");

/***/ }),
/* 7 */
/***/ (function(module, exports) {

module.exports = require("socket.io");

/***/ }),
/* 8 */
/***/ (function(module, exports) {

module.exports = require("path");

/***/ }),
/* 9 */
/***/ (function(module, exports) {

module.exports = require("uuid");

/***/ }),
/* 10 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Particle__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__Vector__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__Game__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__Player__ = __webpack_require__(2);





class Ball extends __WEBPACK_IMPORTED_MODULE_0__Particle__["a" /* default */] {
	constructor( position, velocity ) {
		super( position, velocity );
	}

	checkCollisionsAndSetPosition( playerOne, playerTwo ) {
		let new_position = this.position.add( this.velocity );

		// Check collision with Player One
		if( new_position.x <= __WEBPACK_IMPORTED_MODULE_3__Player__["a" /* default */].DEFAULT_WIDTH * 2 ) {
			if( new_position.y - Ball.DEFAULT_RADIUS >= playerOne.position.y && new_position.y <= playerOne.position.y + __WEBPACK_IMPORTED_MODULE_3__Player__["a" /* default */].DEFAULT_HEIGHT ) {
				if( new_position.x + Ball.DEFAULT_RADIUS <= ( playerOne.position.x + __WEBPACK_IMPORTED_MODULE_3__Player__["a" /* default */].DEFAULT_WIDTH ) && new_position.x + Ball.DEFAULT_RADIUS >= __WEBPACK_IMPORTED_MODULE_3__Player__["a" /* default */].DEFAULT_STEP ) {
					// Player One hit the ball
					new_position.x = playerOne.position.x + __WEBPACK_IMPORTED_MODULE_3__Player__["a" /* default */].DEFAULT_WIDTH + Ball.DEFAULT_RADIUS;
					this.velocity.x *= -1;
				}
			}
		}

		// Check collision with Player Two
		if( new_position.x >= __WEBPACK_IMPORTED_MODULE_2__Game__["a" /* default */].CANVAS_WIDTH - __WEBPACK_IMPORTED_MODULE_3__Player__["a" /* default */].DEFAULT_WIDTH * 2 ) {
			if( new_position.y - Ball.DEFAULT_RADIUS >= playerTwo.position.y && new_position.y <= playerTwo.position.y + __WEBPACK_IMPORTED_MODULE_3__Player__["a" /* default */].DEFAULT_HEIGHT ) {
				if( new_position.x - Ball.DEFAULT_RADIUS <= playerTwo.position.x && new_position.x - Ball.DEFAULT_RADIUS <= __WEBPACK_IMPORTED_MODULE_2__Game__["a" /* default */].CANVAS_WIDTH - __WEBPACK_IMPORTED_MODULE_3__Player__["a" /* default */].DEFAULT_STEP ) {
					// Player Two hit the ball
					new_position.x = playerTwo.position.x - Ball.DEFAULT_RADIUS;
					this.velocity.x *= -1;
				}
			}
		}

		if( new_position.x + Ball.DEFAULT_RADIUS >= __WEBPACK_IMPORTED_MODULE_2__Game__["a" /* default */].CANVAS_WIDTH ) {
			new_position.x = __WEBPACK_IMPORTED_MODULE_2__Game__["a" /* default */].CANVAS_WIDTH - Ball.DEFAULT_RADIUS - __WEBPACK_IMPORTED_MODULE_3__Player__["a" /* default */].DEFAULT_STEP;
			this.velocity.x *= -1;
			// Score increment for Player One
			playerOne.score++;
		} else if( new_position.x <= 0 ) {
			new_position.x = Ball.DEFAULT_RADIUS + __WEBPACK_IMPORTED_MODULE_3__Player__["a" /* default */].DEFAULT_STEP;
			this.velocity.x *= -1;
			// Score increment for Player Two
			playerTwo.score++;
		}

		if( new_position.y + Ball.DEFAULT_RADIUS >= __WEBPACK_IMPORTED_MODULE_2__Game__["a" /* default */].CANVAS_HEIGHT ) {
			new_position.y = __WEBPACK_IMPORTED_MODULE_2__Game__["a" /* default */].CANVAS_HEIGHT - Ball.DEFAULT_RADIUS;
			this.velocity.y *= -1;
		} else if( new_position.y <= 0 ) {
			new_position.y = Ball.DEFAULT_RADIUS;
			this.velocity.y *= -1;
		}

		this.position = new_position;
	}

	static getRandomVelocity() {
		const randomX = Math.floor(Math.random() * 3) -1;
		const randomY = Math.floor(Math.random() * 3) -1;
		const x =  (randomX) ? randomX : 1;
		const y = (randomY) ? randomY : 1;
		return new __WEBPACK_IMPORTED_MODULE_1__Vector__["a" /* default */]( x * Ball.DEFAULT_SPEED, y * Ball.DEFAULT_SPEED);
	}
}
/* harmony export (immutable) */ __webpack_exports__["a"] = Ball;


Ball.DEFAULT_X = 300;
Ball.DEFAULT_Y = 250;
Ball.DEFAULT_RADIUS = 10;
Ball.DEFAULT_START_ANGLE = 0;
Ball.DEFAULT_END_ANGLE = 2*Math.PI;
Ball.DEFAULT_SPEED = 5;
Ball.DEFAULT_X_DIRECTION = 1;
Ball.DEFAULT_Y_DIRECTION = 1;

/***/ })
/******/ ]);