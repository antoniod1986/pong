# README #

Pong

### What is this repository for? ###

This is a pong game developed in html5, using my own VanillaJS framework, NodeJS, SocketIO and Webpack. 

### How do I get set up? ###

git clone git@bitbucket.org:antoniod1986/pong.git   
cd pong   
npm install

Open the terminal and run:     
npm run serve

It is also possible to build both server and client app running:
npm run build
