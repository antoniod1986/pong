export default class Particle {
	constructor( position, velocity ) {
		this.position = position;
		this.velocity = velocity;
	}

	checkCollisionsAndSetPosition() {
		// override
	}
}