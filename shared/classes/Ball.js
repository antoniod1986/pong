import Particle from "./Particle";
import Vector from "./Vector";
import Game from "./Game";
import Player from "./Player";

export default class Ball extends Particle {
	constructor( position, velocity ) {
		super( position, velocity );
	}

	checkCollisionsAndSetPosition( playerOne, playerTwo ) {
		let new_position = this.position.add( this.velocity );

		// Check collision with Player One
		if( new_position.x <= Player.DEFAULT_WIDTH * 2 ) {
			if( new_position.y - Ball.DEFAULT_RADIUS >= playerOne.position.y && new_position.y <= playerOne.position.y + Player.DEFAULT_HEIGHT ) {
				if( new_position.x + Ball.DEFAULT_RADIUS <= ( playerOne.position.x + Player.DEFAULT_WIDTH ) && new_position.x + Ball.DEFAULT_RADIUS >= Player.DEFAULT_STEP ) {
					// Player One hit the ball
					new_position.x = playerOne.position.x + Player.DEFAULT_WIDTH + Ball.DEFAULT_RADIUS;
					this.velocity.x *= -1;
				}
			}
		}

		// Check collision with Player Two
		if( new_position.x >= Game.CANVAS_WIDTH - Player.DEFAULT_WIDTH * 2 ) {
			if( new_position.y - Ball.DEFAULT_RADIUS >= playerTwo.position.y && new_position.y <= playerTwo.position.y + Player.DEFAULT_HEIGHT ) {
				if( new_position.x - Ball.DEFAULT_RADIUS <= playerTwo.position.x && new_position.x - Ball.DEFAULT_RADIUS <= Game.CANVAS_WIDTH - Player.DEFAULT_STEP ) {
					// Player Two hit the ball
					new_position.x = playerTwo.position.x - Ball.DEFAULT_RADIUS;
					this.velocity.x *= -1;
				}
			}
		}

		if( new_position.x + Ball.DEFAULT_RADIUS >= Game.CANVAS_WIDTH ) {
			new_position.x = Game.CANVAS_WIDTH - Ball.DEFAULT_RADIUS - Player.DEFAULT_STEP;
			this.velocity.x *= -1;
			// Score increment for Player One
			playerOne.score++;
		} else if( new_position.x <= 0 ) {
			new_position.x = Ball.DEFAULT_RADIUS + Player.DEFAULT_STEP;
			this.velocity.x *= -1;
			// Score increment for Player Two
			playerTwo.score++;
		}

		if( new_position.y + Ball.DEFAULT_RADIUS >= Game.CANVAS_HEIGHT ) {
			new_position.y = Game.CANVAS_HEIGHT - Ball.DEFAULT_RADIUS;
			this.velocity.y *= -1;
		} else if( new_position.y <= 0 ) {
			new_position.y = Ball.DEFAULT_RADIUS;
			this.velocity.y *= -1;
		}

		this.position = new_position;
	}

	static getRandomVelocity() {
		const randomX = Math.floor(Math.random() * 3) -1;
		const randomY = Math.floor(Math.random() * 3) -1;
		const x =  (randomX) ? randomX : 1;
		const y = (randomY) ? randomY : 1;
		return new Vector( x * Ball.DEFAULT_SPEED, y * Ball.DEFAULT_SPEED);
	}
}

Ball.DEFAULT_X = 300;
Ball.DEFAULT_Y = 250;
Ball.DEFAULT_RADIUS = 10;
Ball.DEFAULT_START_ANGLE = 0;
Ball.DEFAULT_END_ANGLE = 2*Math.PI;
Ball.DEFAULT_SPEED = 5;
Ball.DEFAULT_X_DIRECTION = 1;
Ball.DEFAULT_Y_DIRECTION = 1;