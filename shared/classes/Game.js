export default class Game {
	constructor( playerOne, playerTwo, room, ball ) {
		this.playerOne = playerOne;
		this.playerTwo = playerTwo;
		this.room = room;
		this.ball = ball;
	}

	init() {
		const that = this;
		setTimeout(function() {

		},
			Game.FPS);
	}
}

Game.FPS = 1000/60;
Game.CANVAS_WIDTH = 500;
Game.CANVAS_HEIGHT = 400;