export default class Vector {
	constructor( x = 0, y = 0, module ) {
		this.x = x;
		this.y = y;
	}

	negative() {
		return new Vector( -this.x, -this.y );
	}

	add( v ) {
		return new Vector( this.x + v.x, this.y + v.y );
	}

	substract ( v ) {
		return new Vector( this.x + v.x, this.y + v.y );
	}

	multiply ( v ) {
		return new Vector( this.x * v.x, this.y * v.y );
	}

	divide ( v ) {
		return new Vector( this.x / v.x, this.y / v.y );
	}
}