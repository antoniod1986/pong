import Particle from "./Particle";
import Vector from "./Vector";
import Game from "./Game";

export default class Player extends Particle {
	constructor(position, velocity, sockID, user, roomID) {
		super(position, velocity);
		this.sockID = sockID;
		this.user = user;
		this.roomID = roomID;
		this.score = 0;
	}

	checkCollisionsAndSetPosition(direction) {
		let new_velocity = new Vector( 0, Player.DEFAULT_STEP * direction );
		let new_position = this.position.add( new_velocity );

		if( Player.DEFAULT_HEIGHT + new_position.y >= Game.CANVAS_HEIGHT ) {
			new_position.y = Game.CANVAS_HEIGHT - Player.DEFAULT_HEIGHT - Player.DEFAULT_STEP;
		} else if( new_position.y <= 0 ) {
			new_position.y = Player.DEFAULT_STEP;
		}

		this.position = new_position;
	}
}

Player.DEFAULT_WIDTH = 15;
Player.DEFAULT_HEIGHT = 80;
Player.DEFAULT_X_PLAYER_ONE = 5;
Player.DEFAULT_X_PLAYER_TWO = 480;
Player.DEFAULT_Y = 160;
Player.DEFAULT_STEP = 5;